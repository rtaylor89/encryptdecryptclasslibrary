using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace EncryptDecrypt
{
    class Program
    {
        static void Main(string[] args)
        {
            var encryptedinput = "";
            var decryptedInput = "";
            var originalInput = "";

            // declaring key
            //var key = "b14ca5898a4e4133bbce2ea2315a1916";
            var key =   "ha7cb5835a4e4177bbce2ea4316j7423";

            try
            {
                if (args.Length < 1)
                {
                    Console.WriteLine("Usage: EncryptDecrypt [-encrypt or -decrypt] [password]");
                    System.Environment.Exit(0);
                }

                if (args[0].ToLower() == "-encrypt")
                {
                    // encrypt parameters
                    encryptedinput = EncryptDecryptClassLibrary.EncryptDecrypt.EncryptString(key, args[1]);
                    decryptedInput = EncryptDecryptClassLibrary.EncryptDecrypt.DecryptString(key, encryptedinput);
                    originalInput = args[1];


                }

                if (args[0].ToLower() == "-decrypt")
                {
                    // encrypt parameters
                    decryptedInput = EncryptDecryptClassLibrary.EncryptDecrypt.DecryptString(key, args[1].Trim());
                    encryptedinput = EncryptDecryptClassLibrary.EncryptDecrypt.EncryptString(key, decryptedInput);
                    originalInput = args[1];
                }

                Console.WriteLine("Encrypted Input: " + encryptedinput);
                Console.WriteLine("Decrypted Input: " + decryptedInput);
                Console.WriteLine("Original Input: " + args[1]);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error -- " + e.Message);
            }
        }
    }
}
